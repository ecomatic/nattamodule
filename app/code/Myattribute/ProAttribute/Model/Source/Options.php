<?php
namespace Myattribute\ProAttribute\Model\Source;

use Magento\Eav\Model\Entity\Attribute\Source\AbstractSource;
class Options extends AbstractSource
{
    public function getAllOptions()
    {
            
            $this->_options = [
                ['label'=>__('Small'), 'value'=>'small'],
                ['label'=>__('Medium'), 'value'=>'medium'],
                ['label'=>__('Big'), 'value'=>'big'],
            ];
            return $this->_options;
    }

}
