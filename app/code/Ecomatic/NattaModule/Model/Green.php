<?php
namespace Ecomatic\NattaModule\Model;

use Ecomatic\NattaModule\Api\Color;

class Green implements Color
{
    public function getColor()
    {
        return 'Green';
    }
}