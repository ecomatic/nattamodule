<?php
namespace Ecomatic\NattaModule\Model;

use Ecomatic\NattaModule\Api\PencilInterface;
use Ecomatic\NattaModule\Api\Color;
use Ecomatic\NattaModule\Api\Size;

class Pencil implements PencilInterface
{
    protected $color;
    protected $size;

    public function __construct( Color $color, Size $size)
    {
        
        $this->color = $color;
        $this->size = $size;
    }

    public function getPencilType()
    {
        return 'This pencil is ' . $this->color->getColor() . ' Color and ' . $this->size->getSize() . ' size';
    }
}
