<?php
namespace Ecomatic\NattaModule\Model;

use Ecomatic\NattaModule\Api\Size;

class Normal implements Size
{
    public function getSize()
    {
        return 'Normal';
    }
}