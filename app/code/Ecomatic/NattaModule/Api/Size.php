<?php
namespace Ecomatic\NattaModule\Api;

interface Size 
{
    public function getSize();
}
