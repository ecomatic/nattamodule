<?php
namespace Ecomatic\NattaModule\Model;

use Ecomatic\NattaModule\Api\Size;

class Small implements Size
{
    public function getSize()
    {
        return 'Small';
    }
}