<?php
namespace Ecomatic\NattaModule\Model;

use Ecomatic\NattaModule\Api\Size;

class Big implements Size
{
    public function getSize()
    {
        return 'Big';
    }
}