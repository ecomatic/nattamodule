<?php
namespace Ecomatic\NattaModule\Model;

use Ecomatic\NattaModule\Api\Color;

class Yellow implements Color
{
    public function getColor()
    {
        return 'Yellow';
    }
}
