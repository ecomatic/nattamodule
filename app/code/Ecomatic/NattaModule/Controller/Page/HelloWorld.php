<?php
namespace Ecomatic\NattaModule\Controller\Page;

 use Magento\Framework\App\ResponseInterface;
 //use Ecomatic\NattaModule\NotMagento\PencilInterface;
 use Ecomatic\NattaModule\Api\PencilInterface;
 use Magento\Catalog\Api\ProductRepositoryInterface;
  

class HelloWorld extends \Magento\Framework\App\Action\Action
{
     protected $pencilInterface;
     protected $productRepository;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
         PencilInterface $pencilInterface,
         ProductRepositoryInterface $productRepository )
	{
        $this->pencilInterface = $pencilInterface;
        $this->productRepository = $productRepository;
        
		return parent::__construct($context);
	}
    public function execute()
    {
       echo 'Natta Module Saying Hello world'. "<br>"; 
        echo $this->pencilInterface->getPencilType(). "<br>"; 
        echo get_class($this->productRepository). "<br>";
        
        //$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        //$pencil = $objectManager->create( type:'Ecomatic\NattaModule\Model\Pencil');

        //$student = $objectManager->create(type:'Ecomatic\NattaModule\Model\Student');
        //var_dump($student);
    }
}