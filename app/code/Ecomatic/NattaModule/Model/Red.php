<?php
namespace Ecomatic\NattaModule\Model;

use Ecomatic\NattaModule\Api\Color;

class Red implements Color
{
    public function getColor()
    {
        return 'Red';
    }
}